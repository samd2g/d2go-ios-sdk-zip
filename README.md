# D2GOSDK

[![Version](https://img.shields.io/cocoapods/v/Onfido.svg?style=flat)](http://cocoapods.org/pods/D2GOSDK)
[![License](https://img.shields.io/cocoapods/l/Onfido.svg?style=flat)](http://cocoapods.org/pods/D2GOSDK)
[![Platform](https://img.shields.io/cocoapods/p/Onfido.svg?style=flat)](http://cocoapods.org/pods/D2GOSDK)

[Digital2GO](https://www.digital2go.com/) is a location-based media, marketing & mobile technology company focused on redefining CONSUMER ENGAGEMENT at physical locations and beyond. This SDK provides an easy way to integrate with our 
API, allowing you to build a global consumer engagement platform using proximity.

## Definitions

Throughout this guide, these terms will be used:
"D2GO Cloud Platform" - the Digital2GO backend that contains the configuration for campaigns, applications, etc.;
"app" or "client app" - the client application that uses our Digital2GO SDK to connect with the D2GO Cloud Platform;
"D2GO API" - the backend functions that the SDK calls on behalf of the client app, such as geofence and beacon proximity.

## Overview

The SDK provides a drop-in integration for iOS apps and includes the following features:

1. Log in to an account that you create with Digital2GO on its D2GO Cloud Platform
2. Get a list of surrounding geofences and submit proximity requests to the D2GO API
3. Get Ad objects user enters/exits geofences or comes into proximity to beacon regions you set up on the D2GO Cloud Platform
4. Present your client app users with the ads received from the D2GO API

By using the SDK you don't need to implement the details of beacon and geofence proximity, saving on development time and you can leverage our proximity and session validation to ensure good quality user experience.

## Setup

The SDK is availble on Cocoapods and you can include it in your projects by adding it to your Podfile:

```ruby
pod 'D2GOSDK'
```

## Usage

After adding the SDK as a dependency of your project and running `pod install`, you can import the D2GOSDK in your App Delegate and other classes and interact with our D2GO API.

You should start by implementing the `D2GODelegate` protocol in your AppDelegate then calling the `D2GOClient` login setup with the `setAPILoginRecord()` method, as shown below:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.


    if !D2GOClient.shared.didInitializeAppTokens() {
        //Code execution will get here if this is the first time this app is launched with the D2GOSDK or 
        //if an error has occurred (refer to D2GClientReturns.badAPIcredentials case in the sample client app). 
        //In that case, the developer needs to use their Digital2GO assigned app user name and password.
        //However, note that developers are advised NOT to store their app user name and password on their device
        //but rather store them at a different location (such as a backend server) and fetch them only when needed (such as in this case).
        
        //Recommended call sequence:
        //  fetchAppUserNameAndPasswordFromBackend()
        //  D2GOClient.shared.setAPILoginRecord(username: <app user name>, password: <app password>)
        D2GOClient.shared.setAPILoginRecord(username: "<app id assigned by D2GO Cloud>", password: "<password>")
    }



    return D2GOClient.shared.didFinishLaunching(application, withOptions: launchOptions)
}
```

Other methods in the App Delegate are also routed through their corresponding `D2GOClient` methods. This includes: `handleEventsForBackgroundURLSession`, `sourceApplication`, and `applicationDidBecomeActive`. Refer to sample application for more details.

### View Controller

In your ViewController class(es), do the following:

`import D2GOSDK` and `import CoreLocation`. 

Create an for your View Controller and implement the `D2GOClientDelegate` protocol as well as the following two methods, as follows:


```swift
extension ViewController : D2GOClientDelegate {
    func didFindNewOfferAd(ad: Ad) {
        //Include code to handle when new offers are found by the SDK
        //for example get details from the Ad's public properties/methods
        //and display them to your user. Refer to sample client app.
    }

    func handleD2GClientReturns(clientReturn: D2GClientReturns, monitoredEntity: MonitoredEntity?) {
        switch clientReturn {

        //Login Codes/Errors. Refer to the full list of return codes in the sample client app
        case .loginServerError:
            print("An error was received from the API Login Server")
        case .noLocationDetected:
            print("Please allow app to access your location.") 
            :
            :
        case .badAPIcredentials:
            print("Ann error occurred with your API credentials. Refresh your credentials and try again")
            D2GOClient.shared.setAPILoginRecord(username: "<app id assigned by D2GO Cloud>", password: "<password>")
            D2GOClient.shared.refreshAPICredentials(monitoredEntity: monitoredEntity)
            //Retry logic
            
    }
```


In `ViewDidLoad()`, enter the following calls to set your controller to (respectively) be a D2GO client delegate, request device authorization, verify your API login and set up the AWS login to be able to log in to the app, using the built-in AWS login infrastructure:

```swift
D2GOClient.shared.d2gClientDelegate = self
D2GOClient.shared.requestInitialDeviceManagerAuth()
D2GOClient.shared.verifyLogin()
D2GOClient.shared.setupAWS()
```

### API Token

You can ask for your API Token by contacting our support team or through your account manager and you can signup [here](https://www.digital2go.com/register) if you're not a client yet.

### Digital2GO API

The SDK also includes Swift bindings for all the endpoints currently supported by our API: registration, geofence listing, as well as geofence and beacon proximity. You can find more details about these endpoints in our [documentation](https://dev.api.digital2go.com/docs/index.html).



Copyright 2017 Digital2GO, Inc. All rights reserved.
