# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
This change log file is based on best practices from [Keep a CHANGELOG](http://keepachangelog.com/)


## [1.0.21] - 2017-1-26
### Changed
- Fixed issues that were caused by updgrading to AWS in 1.0.20

## [1.0.20] - 2017-1-26
### Changed
- Upgraded to latest version of AWS packages


## [1.0.19] - 2017-1-26
### Changed
- Fixed the issue with mobile hub helper client

## [1.0.17] - 2017-1-25
### Changed
- Fixed error due to bad credentials by setting login record again and calling D2GOClient's refreshAPICredentials

## [1.0.14] - 2017-1-15

### Changed
- Fixed some error conditions and added documentation

## [1.0.11] - 2017-1-5

### Changed
- Crucial fixes to the framework and thinned content

## [1.0.10] - 2017-1-2
### Added
- Initial public release (preview)
