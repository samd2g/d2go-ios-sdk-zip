//
//  D2GOSDK.h
//  D2GOSDK
//
//  Created by Samir Tout on 11/20/16.
//  Copyright © 2016 Mars Tech LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for D2GOSDK.
FOUNDATION_EXPORT double D2GOSDKVersionNumber;

//! Project version string for D2GOSDK.
FOUNDATION_EXPORT const unsigned char D2GOSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <D2GOSDK/PublicHeader.h>


